from django.shortcuts import render
from django.views.generic import View, TemplateView, CreateView, UpdateView, UpdateView, DeleteView, ListView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.db.models import Q

import datetime
from dateutil.relativedelta import relativedelta
from .models import Book, Borrow
from .forms import BookForm

class BookCreateView(LoginRequiredMixin, CreateView):
    model = Book
    form_class = BookForm

    def form_valid(self, form):
        form.instance.uploader = self.request.user
        if self.request.user.is_staff:
            form.instance.is_approved = True
        return super(BookCreateView, self).form_valid(form)


class BookDeleteView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        book = Book.objects.get(pk=self.kwargs['pk'])
        if self.request.user == book.uploader:
            book.delete()
        else:
            raise PermissionDenied
        return redirect('book_list')


class BookDetailView(DetailView):
    model = Book


class BookUpdateView(LoginRequiredMixin, UpdateView):
    model = Book
    form_class = BookForm

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_staff:
            if request.user != self.get_object().uploader:
                raise PermissionDenied
        return super(BookUpdateView, self).dispatch(request, *args, **kwargs)


class BorrowReturnView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        borrow = Borrow.objects.get(pk=self.kwargs['pk'])
        if self.request.user.is_staff:
            borrow.returned = True
            borrow.save()
        else:
            raise PermissionDenied
        return redirect('borrow_list')



class BookListView(ListView):
    model = Book
    paginate_by = 10

    def get_queryset(self):
        try:
            search = self.request.GET.get('search',)
        except:
            search = None
        if search:
            query = Q(title__icontains=search)
            query.add(Q(isbn__icontains=search), Q.OR)
            query.add(Q(author__icontains=search), Q.OR)
            query.add(Q(publisher__icontains=search), Q.OR)
            query.add(Q(year__icontains=search), Q.OR)
            book_list = Book.objects.filter(query)
        else:
            book_list = Book.objects.all()
        return book_list

class BookUploadedView(LoginRequiredMixin, ListView):
    model = Book
    paginate_by = 5
    template_name = 'books/book_uploaded.html'

    def get_queryset(self):
        return self.model.objects.filter(uploader=self.request.user)


class BorrowCreateView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        book = Book.objects.get(pk=self.kwargs['isbn'])
        if not book.is_borrowed:
            borrow = Borrow.objects.create(
                return_date=datetime.date.today() + relativedelta(months=1),
                borrowed_book=book,
                borrower=self.request.user
            )
            book.is_borrowed = True
        return redirect('book_list')


class BorrowListView(LoginRequiredMixin, ListView):
    model = Borrow

    def get_queryset(self):
        if self.request.user.is_staff:
            return self.model.objects.all()
        else:
            return self.model.objects.filter(borrower=self.request.user)
