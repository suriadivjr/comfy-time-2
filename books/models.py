from django.db import models
from django.conf import settings
from django.urls import reverse
from isbn_field import ISBNField

import datetime
from dateutil.relativedelta import relativedelta

class Book(models.Model):

    # Fields
    isbn        = ISBNField(primary_key=True)
    title       = models.CharField(max_length=100)
    author      = models.CharField(max_length=100)
    publisher   = models.CharField(max_length=100, null=True)
    year        = models.PositiveSmallIntegerField(null=True)
    description = models.TextField(max_length=1000)
    cover_img   = models.URLField(null=True)
    ebook_link  = models.URLField(null=True)
    is_approved = models.BooleanField(default=False)
    is_borrowed = models.BooleanField(default=False)
    uploaded    = models.DateTimeField(auto_now_add=True)


    # Relations
    uploader    = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='books'
    )

    class Meta:
        ordering = ['-uploaded']
        verbose_name_plural = 'books'

    def __str__(self):
        return '%s - %s' % (self.pk, self.title)

    def get_absolute_url(self):
        return reverse('book_detail', args=[self.pk])

    def get_update_url(self):
        return reverse('book_update', args=[self.pk])
    
    def get_delete_url(self):
        return reverse('book_delete', args=[self.pk])


class Borrow(models.Model):

    # Fields
    borrow_date = models.DateField(auto_now_add=True)
    return_date = models.DateField(null=False)
    returned    = models.BooleanField(default=False)

    # Relations
    borrowed_book = models.ForeignKey(
        'books.book',
        on_delete=models.CASCADE,
        related_name='borrows',
    )

    borrower = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='borrows'
    )

    def save(self, *args, **kwargs):
        self.borrowed_book.is_borrowed = not self.returned
        self.borrowed_book.save()
        return super(Borrow, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-pk']
        verbose_name_plural = 'borrows'

    def __str__(self):
        return '%s - %s' % (self.borrowed_book.isbn, self.borrower)

    def get_absolute_url(self):
        return reverse('borrow_detail', args=[self.pk])