from django.contrib import admin
from django import forms
from .models import Book, Borrow

class BookAdminForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = '__all__'

class BookAdmin(admin.ModelAdmin):
    form = BookAdminForm

admin.site.register(Book, BookAdmin)

class BorrowAdminForm(forms.ModelForm):
    class Meta:
        model = Borrow
        fields = '__all__'

class BorrowAdmin(admin.ModelAdmin):
    form = BorrowAdminForm

admin.site.register(Borrow, BorrowAdmin)