from django.urls import path
from . import views

urlpatterns = [
    path('books/', views.BookListView.as_view(), name='book_list'),
    path('books/uploaded/', views.BookUploadedView.as_view(), name='book_uploaded'),
    path('books/create/', views.BookCreateView.as_view(), name='book_create'),
    path('books/<slug:pk>', views.BookDetailView.as_view(), name='book_detail'),
    path('books/update/<slug:pk>', views.BookUpdateView.as_view(), name='book_update'),
    path('books/delete/<slug:pk>', views.BookDeleteView.as_view(), name='book_delete'),
]

urlpatterns += [
    path('borrows/', views.BorrowListView.as_view(), name='borrow_list'),
    path('borrows/create/<slug:isbn>', views.BorrowCreateView.as_view(), name='borrow_create'),
    path('borrows/return/<slug:pk>', views.BorrowReturnView.as_view(), name='borrow_return'),
]