from django import forms

from .models import Book

class BookForm(forms.ModelForm):

    class Meta:
        model = Book
        fields = ['isbn', 'title', 'publisher', 'author', 'year', 'description', 'cover_img', 'ebook_link']

    def __init__(self, *args, **kwargs):
        super(BookForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
        self.fields['description'].widget.attrs['class'] += ' md-textarea'
        self.fields['description'].widget.attrs['rows'] = '3'